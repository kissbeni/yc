﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace yakinducode
{
    public partial class SourceParser
    {
        public class Choice
        {
            public Transition SourceTransition;
            public Block DefaultTarget;
            public List<Transition> OutgoingTransitions = new List<Transition>();
            public String UID;

            public Choice()
            {
                UID = Utils.RandomUID();
            }
        }

        private void ProcessChoices()
        {
            foreach (Block region in Regions)
            {
                foreach (Block state in region.SubBlocks.Where(x => x.Type == BlockType.State))
                {
                    List<Block> remove_from_state = new List<Block>();

                    foreach (Block choice in state.SubBlocks.Where(x => x.Type == BlockType.Choice))
                    {
                        Block default_choice = choice.SubBlocks.FirstOrDefault(x => x.Type == BlockType.DefaultChoice);
                        Block @default;

                        if (default_choice == null)
                            @default = state;
                        else
                            @default = region.SubBlocks.First(x => x.Name == default_choice.RealizeContents());

                        Choice ch = new Choice()
                        {
                            SourceTransition = new Transition()
                            {
                                SourceBlock = state,
                                DestinationBlock = choice,
                                Condition = new Block(BlockType.Condition, null, state, choice.RealizeContents())
                            },
                            DefaultTarget = @default
                        };

                        ch.SourceTransition.WhatToDo.AddRange(choice.SubBlocks.Where(x => x.Type == BlockType.Operation));

                        foreach (Block cond in choice.SubBlocks.Where(x => x.Type == BlockType.Condition))
                        {
                            Transition tr = new Transition();
                            tr.Condition = cond;
                            tr.SourceBlock = choice;

                            Block jump = cond.SubBlocks.FirstOrDefault(x => x.Type == BlockType.Transition);
                            if (jump != null)
                                tr.DestinationBlock = region.SubBlocks.First(x => x.Type == BlockType.State && x.Name == jump.Text);
                            else
                                tr.DestinationBlock = state;

                            tr.WhatToDo.AddRange(cond.SubBlocks.Where(x => x.Type != BlockType.Transition));
                            ch.OutgoingTransitions.Add(tr);
                        }

                        Choices.Add(ch);
                        remove_from_state.Add(choice);
                    }

                    foreach (Block blk in remove_from_state)
                        state.SubBlocks.Remove(blk);
                }
            }
        }
    }
}
