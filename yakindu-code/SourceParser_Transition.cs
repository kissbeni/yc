﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace yakinducode
{
    public partial class SourceParser
    {
        public class Transition
        {
            public Block SourceBlock;
            public Block DestinationBlock;
            public String UID;
            public Block Condition;
            public List<Block> WhatToDo = new List<Block>();

            public Transition()
            {
                UID = Utils.RandomUID();
            }
        }

        public static List<Transition> FindAllTransitions(Block region)
        {
            List<Transition> res = new List<Transition>();

            Block entry = region.SubBlocks.FirstOrDefault(x => x.Type == BlockType.Entry);
            if (entry != null)
            {
                Transition tr = new Transition();
                tr.Condition = null;
                tr.SourceBlock = entry;
                tr.DestinationBlock = region.SubBlocks.First(x => x.Type == BlockType.State && x.UID == entry.RealizeContents());
                res.Add(tr);
            }

            foreach (Block state in region.SubBlocks.Where(x => x.Type == BlockType.State))
                foreach (Block cond in state.SubBlocks.Where(x => x.Type == BlockType.Condition))
                {
                    Transition tr = new Transition();
                    tr.Condition = cond;
                    tr.SourceBlock = state;

                    Block jump = cond.SubBlocks.FirstOrDefault(x => x.Type == BlockType.Transition);
                    if (jump != null)
                        tr.DestinationBlock = region.SubBlocks.First(x => x.Type == BlockType.State && x.Name == jump.Text);
                    else
                        tr.DestinationBlock = state;

                    tr.WhatToDo.AddRange(cond.SubBlocks.Where(x => x.Type != BlockType.Transition));
                    res.Add(tr);
                }

            return res;
        }
    }
}
