﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace yakinducode
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            SourceParser p = new SourceParser("../../../../hw.yc");
            SourceParser.Block blk = p.RootBlock;
            blk.Print(Console.Out);

            SourceParser.Block specBlock = blk.SubBlocks.First(x => x.Type == SourceParser.BlockType.Specification);
            String spec = SourceParser.YakindufySpecification(specBlock);
            Console.WriteLine(spec);

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = "  ";
            settings.CloseOutput = true;
            settings.OmitXmlDeclaration = true;

            using (XmlWriter writer = XmlWriter.Create("out.sct", settings))
            {
                writer.WriteRaw("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.WriteStartElement("xmi", "XMI", "http://www.omg.org/XMI");
                writer.WriteXmiAttribute("version", "2.0");
                writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
                writer.WriteAttributeString("xmlns", "notation", null, "http://www.eclipse.org/gmf/runtime/1.0.2/notation");
                writer.WriteAttributeString("xmlns", "sgraph", null, "http://www.yakindu.org/sct/sgraph/2.0.0");

                writer.WriteStartElement("sgraph", "Statechart", "http://www.yakindu.org/sct/sgraph/2.0.0");
                writer.WriteXmiAttribute("id", specBlock.UID);
                writer.WriteAttributeString("specification", spec);
                writer.WriteAttributeString("name", specBlock.RealizeContents());

                foreach (SourceParser.Block region in p.Regions)
                {
                    List<SourceParser.Transition> transitions = (List<SourceParser.Transition>)(region.Data = SourceParser.FindAllTransitions(region));

                    writer.WriteStartElement("regions");
                    writer.WriteXmiAttribute("id", region.UID);
                    writer.WriteAttributeString("name", region.RealizeContents());

                    foreach (SourceParser.Block state in region.SubBlocks)
                    {
                        writer.WriteStartElement("vertices");
                        writer.WriteXmiAttribute("id", state.UID);
                        writer.WriteXsiAttribute("type", "sgraph:" + state.Type.ToString());

                        /*String incoming = "";
                        List<String> listed = new List<String>();
                        foreach (SourceParser.Transition t in transitions)
                            if (t.DestinationBlock == state && !listed.Contains(t.SourceBlock.UID))
                            {
                                incoming += (incoming.Length == 0 ? "" : " ") + t.SourceBlock.UID;
                                listed.Add(t.SourceBlock.UID);
                            }

                        if (incoming.Length > 0)
                            writer.WriteAttributeString("incomingTransitions", incoming);*/

                        if (state.Type == SourceParser.BlockType.State)
                        {
                            writer.WriteAttributeString("name", state.RealizeContents());
                            String s = SourceParser.YakindufyState(state);
                            if (!String.IsNullOrEmpty(s))
                                writer.WriteAttributeString("specification", s);
                        }

                        /*foreach (SourceParser.Transition t in transitions)
                            if (t.SourceBlock.UID == state.UID)
                            {
                                Console.WriteLine("Assingning outgoing transition ({0}->{1}) with uid={2}", t.SourceBlock.UID, t.DestinationBlock.UID, t.UID);
                                writer.WriteStartElement("outgoingTransitions");
                                writer.WriteXmiAttribute("id", t.UID);
                                String st = SourceParser.YakindufyTransition(t);
                                if (st != null) writer.WriteAttributeString("specification", st);
                                writer.WriteAttributeString("target", t.DestinationBlock.UID);
                                writer.WriteEndElement();
                            }*/

                        writer.WriteEndElement();
                    }

                    foreach (SourceParser.Choice choice in p.Choices.Where(x => x.SourceTransition.SourceBlock.Parent.UID == region.UID))
                    {
                        writer.WriteStartElement("vertices");
                        writer.WriteXmiAttribute("id", choice.UID);
                        writer.WriteXsiAttribute("type", "sgraph:Choice");
                        writer.WriteEndElement();
                    }

                    writer.WriteEndElement();
                }

                writer.WriteEndElement();


                writer.WriteStartElement("notation", "Diagram", "http://www.eclipse.org/gmf/runtime/1.0.2/notation");
                writer.WriteXmiAttribute("id", Utils.RandomUID());
                writer.WriteAttributeString("element", specBlock.UID);
                writer.WriteAttributeString("measurementUnit", "Pixel");
                writer.WriteAttributeString("type", "org.yakindu.sct.ui.editor.editor.StatechartDiagramEditor");

                foreach (SourceParser.Block region in blk.SubBlocks.Where(x => x.Type == SourceParser.BlockType.Region))
                {
                    writer.WriteStartElement("children");
                    writer.WriteXmiAttribute("id", Utils.RandomUID());
                    writer.WriteAttributeString("type", "Region");
                    writer.WriteAttributeString("element", region.UID);

                    writer.WriteStartElement("children");
                    writer.WriteXmiAttribute("id", Utils.RandomUID());
                    writer.WriteXsiAttribute("type", "notation:DecorationNode");
                    writer.WriteAttributeString("type", "RegionName");

                    writer.WriteStartElement("styles");
                    writer.WriteXmiAttribute("id", Utils.RandomUID());
                    writer.WriteXsiAttribute("type", "notation:ShapeStyle");
                    writer.WriteAttributeString("fontName", "Veranda");
                    writer.WriteEndElement();

                    writer.WriteStartElement("layoutConstraint");
                    writer.WriteXmiAttribute("id", Utils.RandomUID());
                    writer.WriteXsiAttribute("type", "notation:Location");
                    writer.WriteEndElement();

                    writer.WriteEndElement();


                    writer.WriteStartElement("children");
                    writer.WriteXmiAttribute("id", Utils.RandomUID());
                    writer.WriteXsiAttribute("type", "notation:Shape");
                    writer.WriteAttributeString("type", "RegionCompartment");
                    writer.WriteAttributeString("fontName", "Veranda");
                    writer.WriteAttributeString("lineColor", "4210752");

                    foreach (SourceParser.Block state in region.SubBlocks)
                    {
                        writer.WriteStartElement("children");
                        writer.WriteXmiAttribute("id", Utils.RandomUID());
                        writer.WriteAttributeString("type", state.Type.ToString());
                        writer.WriteAttributeString("element", state.UID);

                        if (state.Type == SourceParser.BlockType.Entry)
                        {
                            writer.WriteStartElement("children");
                            writer.WriteXmiAttribute("id", Utils.RandomUID());
                            writer.WriteAttributeString("type", "BorderItemLabelContainer");


                            writer.WriteStartElement("children");
                            writer.WriteXmiAttribute("id", Utils.RandomUID());
                            writer.WriteXsiAttribute("type", "notation:DecorationNode");
                            writer.WriteAttributeString("type", "BorderItemLabel");

                            writer.WriteStartElement("styles");
                            writer.WriteXmiAttribute("id", Utils.RandomUID());
                            writer.WriteXsiAttribute("type", "notation:ShapeStyle");
                            writer.WriteEndElement();

                            writer.WriteStartElement("layoutConstraint");
                            writer.WriteXmiAttribute("id", Utils.RandomUID());
                            writer.WriteXsiAttribute("type", "notation:Location");
                            writer.WriteEndElement();

                            writer.WriteEndElement();


                            writer.WriteStartElement("styles");
                            writer.WriteXmiAttribute("id", Utils.RandomUID());
                            writer.WriteXsiAttribute("type", "notation:ShapeStyle");
                            writer.WriteAttributeString("fontName", "Verdana");
                            writer.WriteAttributeString("lineColor", "4210752");
                            writer.WriteEndElement();

                            writer.WriteStartElement("layoutConstraint");
                            writer.WriteXmiAttribute("id", Utils.RandomUID());
                            writer.WriteXsiAttribute("type", "notation:Bounds");
                            writer.WriteEndElement();

                            writer.WriteEndElement();


                            writer.WriteStartElement("layoutConstraint");
                            writer.WriteXmiAttribute("id", Utils.RandomUID());
                            writer.WriteXsiAttribute("type", "notation:Bounds");
                            writer.WriteAttributeString("x", "200");
                            writer.WriteAttributeString("y", "200");
                            writer.WriteEndElement();
                        }
                        else if (state.Type == SourceParser.BlockType.State)
                        {
                            writer.WriteStartElement("children");
                            writer.WriteXmiAttribute("id", Utils.RandomUID());
                            writer.WriteXsiAttribute("type", "notation:DecorationNode");
                            writer.WriteAttributeString("type", "StateName");

                            writer.WriteStartElement("styles");
                            writer.WriteXmiAttribute("id", Utils.RandomUID());
                            writer.WriteXsiAttribute("type", "notation:ShapeStyle");
                            writer.WriteEndElement();

                            writer.WriteStartElement("layoutConstraint");
                            writer.WriteXmiAttribute("id", Utils.RandomUID());
                            writer.WriteXsiAttribute("type", "notation:Location");
                            writer.WriteEndElement();

                            writer.WriteEndElement();


                            writer.WriteStartElement("children");
                            writer.WriteXmiAttribute("id", Utils.RandomUID());
                            writer.WriteXsiAttribute("type", "notation:Compartment");
                            writer.WriteAttributeString("type", "StateTextCompartment");

                            writer.WriteStartElement("children");
                            writer.WriteXmiAttribute("id", Utils.RandomUID());
                            writer.WriteXsiAttribute("type", "notation:Shape");
                            writer.WriteAttributeString("type", "StateTextCompartmentExpression");
                            writer.WriteAttributeString("fontName", "Verdana");
                            writer.WriteAttributeString("lineColor", "4210752");

                            writer.WriteStartElement("layoutConstraint");
                            writer.WriteXmiAttribute("id", Utils.RandomUID());
                            writer.WriteXsiAttribute("type", "notation:Bounds");
                            writer.WriteEndElement();

                            writer.WriteEndElement();

                            writer.WriteEndElement();


                            writer.WriteStartElement("children");
                            writer.WriteXmiAttribute("id", Utils.RandomUID());
                            writer.WriteXsiAttribute("type", "notation:Compartment");
                            writer.WriteAttributeString("type", "StateFigureCompartment");
                            writer.WriteEndElement();

                            writer.WriteStartElement("styles");
                            writer.WriteXmiAttribute("id", Utils.RandomUID());
                            writer.WriteXsiAttribute("type", "notation:FontStyle");
                            writer.WriteEndElement();

                            writer.WriteStartElement("styles");
                            writer.WriteXmiAttribute("id", Utils.RandomUID());
                            writer.WriteXsiAttribute("type", "notation:BooleanValueStyle");
                            writer.WriteAttributeString("name", "isHorizontal");
                            writer.WriteAttributeString("booleanValue", "true");
                            writer.WriteEndElement();

                            writer.WriteStartElement("layoutConstraint");
                            writer.WriteXmiAttribute("id", Utils.RandomUID());
                            writer.WriteXsiAttribute("type", "notation:Bounds");
                            writer.WriteAttributeString("x", "200");
                            writer.WriteAttributeString("y", "200");
                            writer.WriteAttributeString("height", "100");
                            writer.WriteAttributeString("width", "100");
                            writer.WriteEndElement();
                        }

                        writer.WriteStartElement("styles");
                        writer.WriteXmiAttribute("id", Utils.RandomUID());
                        writer.WriteXsiAttribute("type", "notation:ShapeStyle");
                        writer.WriteAttributeString("fontName", "Verdana");
                        writer.WriteAttributeString("lineColor", "4210752");
                        writer.WriteEndElement();

                        writer.WriteEndElement();
                    }

                    foreach (SourceParser.Choice choice in p.Choices.Where(x => x.SourceTransition.SourceBlock.Parent.UID == region.UID))
                    {
                        writer.WriteStartElement("children");
                        writer.WriteXmiAttribute("id", Utils.RandomUID());
                        writer.WriteXsiAttribute("type", "notation:Shape");
                        writer.WriteAttributeString("type", "Choice");
                        writer.WriteAttributeString("element", choice.UID);
                        writer.WriteAttributeString("fontName", "Veranda");
                        writer.WriteAttributeString("lineColor", "4210752");

                        writer.WriteStartElement("layoutConstraint");
                        writer.WriteXmiAttribute("id", Utils.RandomUID());
                        writer.WriteXsiAttribute("type", "notation:Bounds");
                        writer.WriteAttributeString("x", "200");
                        writer.WriteAttributeString("y", "200");
                        writer.WriteEndElement();

                        writer.WriteEndElement();
                    }

                    writer.WriteEndElement();


                    writer.WriteStartElement("styles");
                    writer.WriteXmiAttribute("id", Utils.RandomUID());
                    writer.WriteXsiAttribute("type", "notation:ShapeStyle");
                    writer.WriteAttributeString("fontName", "Veranda");
                    writer.WriteAttributeString("fillColor", "15790320");
                    writer.WriteAttributeString("lineColor", "12632256");
                    writer.WriteEndElement();

                    writer.WriteStartElement("layoutConstraint");
                    writer.WriteXmiAttribute("id", Utils.RandomUID());
                    writer.WriteXsiAttribute("type", "notation:Bounds");
                    writer.WriteAttributeString("x", "0");
                    writer.WriteAttributeString("y", "0");
                    writer.WriteAttributeString("width", "1234");
                    writer.WriteAttributeString("height", "1234");
                    writer.WriteEndElement();

                    writer.WriteEndElement();
                }

                writer.WriteStartElement("children");
                writer.WriteXmiAttribute("id", Utils.RandomUID());
                writer.WriteXsiAttribute("type", "notation:Shape");
                writer.WriteAttributeString("type", "StatechartText");
                writer.WriteAttributeString("fontName", "Verdana");
                writer.WriteAttributeString("lineColor", "4210752");


                writer.WriteStartElement("children");
                writer.WriteXmiAttribute("id", Utils.RandomUID());
                writer.WriteXsiAttribute("type", "notation:DecorationNode");
                writer.WriteAttributeString("type", "StatechartName");

                writer.WriteStartElement("layoutConstraint");
                writer.WriteXmiAttribute("id", Utils.RandomUID());
                writer.WriteXsiAttribute("type", "notation:Location");
                writer.WriteEndElement();

                writer.WriteStartElement("styles");
                writer.WriteXmiAttribute("id", Utils.RandomUID());
                writer.WriteXsiAttribute("type", "notation:ShapeStyle");
                writer.WriteEndElement();

                writer.WriteEndElement();


                writer.WriteStartElement("children");
                writer.WriteXmiAttribute("id", Utils.RandomUID());
                writer.WriteXsiAttribute("type", "notation:Shape");
                writer.WriteAttributeString("type", "StatechartName");
                writer.WriteAttributeString("fontName", "Verdana");
                writer.WriteAttributeString("lineColor", "4210752");

                writer.WriteStartElement("layoutConstraint");
                writer.WriteXmiAttribute("id", Utils.RandomUID());
                writer.WriteXsiAttribute("type", "notation:Bounds");
                writer.WriteEndElement();

                writer.WriteEndElement();


                writer.WriteStartElement("layoutConstraint");
                writer.WriteXmiAttribute("id", Utils.RandomUID());
                writer.WriteXsiAttribute("type", "notation:Bounds");
                writer.WriteEndElement();


                writer.WriteEndElement();


                writer.WriteStartElement("styles");
                writer.WriteXmiAttribute("id", Utils.RandomUID());
                writer.WriteXsiAttribute("type", "notation:DiagramStyle");
                writer.WriteEndElement();

                writer.WriteStartElement("styles");
                writer.WriteXmiAttribute("id", Utils.RandomUID());
                writer.WriteXsiAttribute("type", "notation:BooleanValueStyle");
                writer.WriteAttributeString("name", "inlineDefinitionSection");
                writer.WriteEndElement();

                foreach (SourceParser.Block region in p.Regions)
                {
                    Console.WriteLine("---- Region {0} ----", region.UID);

                    List<SourceParser.Transition> transitions = (List<SourceParser.Transition>)region.Data;
                    foreach (SourceParser.Transition t in transitions)
                        Console.WriteLine("{0} =({1})=> {2} ({3})", t.SourceBlock.Name, SourceParser.YakindufyTransition(t), t.DestinationBlock.Name, t.UID);

                    foreach (SourceParser.Choice choice in p.Choices.Where(x => x.SourceTransition.SourceBlock.Parent.UID == region.UID))
                    {
                        Console.WriteLine("Transitions of choice {0}:", choice.UID);
                        Console.WriteLine("  Default ==> {0}", choice.DefaultTarget.Name);
                        Console.WriteLine("  {0} =({1})=> <CHOICE> ({2})",
                            choice.SourceTransition.SourceBlock.Name,
                            SourceParser.YakindufyTransition(choice.SourceTransition),
                            choice.SourceTransition.UID);

                        foreach (SourceParser.Transition t in choice.OutgoingTransitions)
                            Console.WriteLine("  <CHOICE> =({0})=> {1} ({2})", SourceParser.YakindufyTransition(t), t.DestinationBlock.Name, t.UID);
                    }
                }

                /*foreach (SourceParser.Block region in p.Regions)
                {
                    List<SourceParser.Transition> transitions = (List<SourceParser.Transition>)region.Data;
                    foreach (SourceParser.Transition t in transitions)
                    {
                        Console.WriteLine("{0} =({1})=> {2} ({3})", t.SourceBlock.Name, SourceParser.YakindufyTransition(t), t.DestinationBlock.Name, t.UID);

                        writer.WriteStartElement("edges");
                        writer.WriteXmiAttribute("id", Utils.RandomUID());
                        writer.WriteAttributeString("type", "Transition");
                        writer.WriteAttributeString("element", t.UID);
                        writer.WriteAttributeString("source", t.SourceBlock.UID);
                        writer.WriteAttributeString("target", t.DestinationBlock.UID);

                        writer.WriteStartElement("children");
                        writer.WriteXmiAttribute("id", Utils.RandomUID());
                        writer.WriteXsiAttribute("type", "notation:DecorationNode");
                        writer.WriteAttributeString("type", "TransitionExpression");

                        writer.WriteStartElement("styles");
                        writer.WriteXmiAttribute("id", Utils.RandomUID());
                        writer.WriteXsiAttribute("type", "notation:ShapeStyle");
                        writer.WriteEndElement();

                        writer.WriteStartElement("layoutConstraint");
                        writer.WriteXmiAttribute("id", Utils.RandomUID());
                        writer.WriteXsiAttribute("type", "notation:Location");
                        writer.WriteAttributeString("x", "200");
                        writer.WriteAttributeString("y", "200");
                        writer.WriteEndElement();

                        writer.WriteEndElement();

                        writer.WriteStartElement("styles");
                        writer.WriteXmiAttribute("id", Utils.RandomUID());
                        writer.WriteXsiAttribute("type", "notation:ConnectorStyle");
                        writer.WriteAttributeString("lineColor", "4210752");
                        writer.WriteEndElement();

                        writer.WriteStartElement("styles");
                        writer.WriteXmiAttribute("id", Utils.RandomUID());
                        writer.WriteXsiAttribute("type", "notation:FontStyle");
                        writer.WriteAttributeString("fontName", "Verdana");
                        writer.WriteEndElement();

                        writer.WriteStartElement("sourceAnchor");
                        writer.WriteXmiAttribute("id", Utils.RandomUID());
                        writer.WriteXsiAttribute("type", "notation:IdentityAnchor");
                        writer.WriteAttributeString("id", "(0.5,0.5)");
                        writer.WriteEndElement();

                        writer.WriteStartElement("targetAnchor");
                        writer.WriteXmiAttribute("id", Utils.RandomUID());
                        writer.WriteXsiAttribute("type", "notation:IdentityAnchor");
                        writer.WriteAttributeString("id", "(0.76875,0.25357142857142856)");
                        writer.WriteEndElement();

                        writer.WriteStartElement("bendpoints");
                        writer.WriteXmiAttribute("id", Utils.RandomUID());
                        writer.WriteXsiAttribute("type", "notation:RelativeBendpoints");
                        writer.WriteAttributeString("points", "[0, 0, 120, -16]$[-118, 16, 2, 0]");
                        writer.WriteEndElement();

                        writer.WriteEndElement();
                    }
                }*/

                writer.WriteEndElement();


                writer.WriteEndElement();
                writer.Flush();
            }
        }
    }
}
