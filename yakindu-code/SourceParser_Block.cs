﻿using System;
using System.Collections.Generic;
using System.IO;

namespace yakinducode
{
    public partial class SourceParser
    {
        public enum BlockType
        {
            Root,
            Entry,
            Specification,
            Interface,
            Internal,
            Region,
            State,
            Condition,
            Variable,
            Constraint,
            Operation,
            Transition,
            StateEntry,
            Choice,
            DefaultChoice,
            Repeat
        };

        public class Block
        {
            public BlockType Type;
            public String Text;
            public String Name;
            public Block Parent;
            public List<Block> SubBlocks;
            public String UID;
            public Object Data;

            public Block(BlockType type, String name, Block parent, String text = null)
            {
                UID = Utils.RandomUID();
                Type = type;
                Text = text;
                Name = name;
                Parent = parent;
                SubBlocks = new List<Block>();
            }

            public void AddChild(Block block)
            {
                SubBlocks.Add(block);
            }

            public String RealizeContents()
            {
                switch (Type)
                {
                    case BlockType.Root: return null;
                    case BlockType.Specification: return Name;
                    case BlockType.Interface: return Name;
                    case BlockType.Variable: return Text;
                    case BlockType.Internal: return null;
                    case BlockType.Region: return Name;
                    case BlockType.Operation: return Text;
                    case BlockType.Condition: return CleanUpCondition(Text);
                    case BlockType.Transition: return "-> " + Text;
                    case BlockType.State: return Name;
                    case BlockType.Entry: return Name;
                    case BlockType.Repeat: return Text;
                    case BlockType.Constraint: return Text;
                    case BlockType.StateEntry: return CleanUpCondition(Text);
                    case BlockType.Choice: return CleanUpCondition(Text);
                    case BlockType.DefaultChoice: return Text;
                    default: return "???";
                }
            }

            public void Print(TextWriter writer, int indentLevel = 0)
            {
                writer.Write(Utils.GenSpaces(indentLevel * 4));
                String c = RealizeContents();

                if (SubBlocks.Count > 0)
                {
                    writer.WriteLine(Type.ToString() + (c != null ? " [" + c + "]" : "") + " {");

                    foreach (Block b in SubBlocks)
                        b.Print(writer, indentLevel + 1);

                    writer.Write(Utils.GenSpaces(indentLevel * 4));
                    writer.WriteLine("}");
                }
                else
                {
                    writer.WriteLine(Type.ToString() + (c != null ? " [" + c + "]" : ""));
                }
            }
        }
    }
}
