﻿using System;
using System.Linq;

namespace yakinducode
{
    public partial class SourceParser
    {
        private static String CleanUpLine(String line)
        {
            int pos = 0;
            char stringChr = '\0';
            char prev = '\0';
            while (pos < line.Length)
            {
                char ch = line[pos];

                if (Char.IsWhiteSpace(ch))
                {
                    if (pos == 0)
                    {
                        line = line.Substring(1);
                        prev = ch;
                        continue;
                    }

                    if (stringChr == '\0')
                    {
                        if (Char.IsWhiteSpace(prev))
                        {
                            String tmp = line.Substring(0, pos) + line.Substring(pos + 1);
                            line = tmp;
                            prev = ch;
                            continue;
                        }

                        if (line.Length > 1 && Utils.CharDoesNotNeedSpace(prev))
                        {
                            String tmp = line.Substring(0, pos) + line.Substring(pos + 1);
                            line = tmp;
                            continue;
                        }
                    }
                }

                if (stringChr == '\0' && line.Length > 1 && !"{}".Contains(prev) && Char.IsWhiteSpace(prev) && Utils.CharDoesNotNeedSpace(ch))
                {
                    String tmp = line.Substring(0, pos - 1) + line.Substring(pos);
                    line = tmp;
                    prev = ch;
                    continue;
                }

                if (ch == '/' && prev == '/')
                {
                    if (pos > 1)
                        line = line.Substring(0, pos - 2);
                    else
                        line = "";
                    break;
                }

                if (stringChr == ch && prev != '\\')
                {
                    stringChr = '\0';
                }
                else if (ch == '\'' || ch == '"')
                {
                    if (stringChr == '\0')
                        stringChr = ch;
                }

                prev = ch;
                pos++;
            }

            while (line.Length > 0 && (Char.IsWhiteSpace(line.Last()) || line.Last() == ';'))
                line = line.Substring(0, line.Length - 1);

            return line;
        }

        private static String CleanUpCondition(String cond)
        {
            int inBracket = 0;
            int startPos = -1;
            int pos = 0;

            while (pos < cond.Length)
            {
                char ch = cond[pos];

                if (inBracket == 0 && ch != '(')
                {
                    pos++;
                    continue;
                }

                pos++;

                if (startPos == -1)
                    startPos = pos;

                if (ch == '(') inBracket++;
                if (ch == ')')
                {
                    inBracket--;
                    if (inBracket == 0)
                        break;
                }
            }

            return startPos == -1 ? cond : cond.Substring(startPos, pos - startPos - 1);
        }
    }
}
