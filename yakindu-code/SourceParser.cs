﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;

namespace yakinducode
{
    public partial class SourceParser
    {
        public String FileName  { get; private set; }
        public Block  RootBlock { get; private set; }
        private List<Block> Constraints = new List<Block>();
        public List<Choice> Choices = new List<Choice>();
        public IEnumerable<Block> Regions { get { return RootBlock.SubBlocks.Where(x => x.Type == BlockType.Region); } }

        public SourceParser(String file)
        {
            FileName = file;
            RootBlock = Process();

            IEnumerable<String> constraints = Constraints.Select(x => x.Name);

            if (constraints.Contains("DISALLOW_CONDITIONAL_TRANSITIONS"))
            {
                int first_run = FixConditionalTransitions();
                int second_run = FixConditionalTransitions();

                Console.WriteLine("DISALLOW_CONDITIONAL_TRANSITIONS constraint summary:");
                Console.WriteLine("  - Found {0} problems in total", first_run);
                Console.WriteLine("  - Successfully fixed {0} problems", first_run - second_run);
                Console.WriteLine("  - {0} problems could not be automatically solved", second_run);
            }

            ProcessChoices();
        }

        private Block Process()
        {
            Block rootBlock = new Block(BlockType.Root, "root", null);
            Block currentBlock = rootBlock;
            Block newBlock;
            Block currentRegion = null;

            int lineNumber = 0;
            using (StreamReader r = new StreamReader(FileName))
            {
                String line;
                bool waitForOpeningBracket = false;

                while ((line = r.ReadLine()) != null)
                {
                    lineNumber++;

                    if (String.IsNullOrWhiteSpace(line = CleanUpLine(line)))
                        continue;

                    if (waitForOpeningBracket && !line.StartsWith("{"))
                        throw new Exception("Missing { before line " + lineNumber);

                    if (line.StartsWith("}"))
                    {
                        if (line.Length > 0)
                            line = CleanUpLine(line.Substring(1));

                        if (currentBlock != null)
                        {
                            if (currentBlock == currentRegion)
                                currentRegion = null;

                            currentBlock = currentBlock.Parent;
                        }
                        else
                            throw new Exception("Too many } near line " + lineNumber);

                        if (String.IsNullOrWhiteSpace(line))
                            continue;
                    }

                    String[] spaceSplit = line.Split('{')[0].Split(' ');

                    if (currentBlock.Type == BlockType.Root)
                    {
                        switch (spaceSplit[0])
                        {
                            case "specification":
                                newBlock = new Block(BlockType.Specification, Utils.ExtractString(line, 0), currentBlock);
                                break;
                            case "region":
                                newBlock = new Block(BlockType.Region, Utils.ExtractString(line, 0), currentBlock);
                                currentRegion = newBlock;
                                break;
                            default:
                                throw new Exception("Invalid keyword in line " + lineNumber);
                        }

                        currentBlock.AddChild(newBlock);
                        currentBlock = newBlock;

                        if (!line.Contains("{"))
                            waitForOpeningBracket = true;
                    }
                    else if (currentBlock.Type == BlockType.Specification)
                    {
                        if (spaceSplit[0] == "constraint")
                        {
                            newBlock = new Block(BlockType.Constraint, Utils.ExtractString(line, 0), currentBlock);
                            currentBlock.AddChild(newBlock);
                            Constraints.Add(newBlock);
                        }
                        else
                        {
                            switch (spaceSplit[0])
                            {
                                case "interface":
                                    newBlock = new Block(BlockType.Interface, spaceSplit.Length > 1 ? spaceSplit[1] : "", currentBlock);
                                    break;
                                case "internal":
                                    newBlock = new Block(BlockType.Internal, spaceSplit.Length > 1 ? spaceSplit[1] : "", currentBlock);
                                    break;
                                default:
                                    throw new Exception("Invalid keyword in line " + lineNumber);
                            }

                            currentBlock.AddChild(newBlock);
                            currentBlock = newBlock;

                            if (!line.Contains("{"))
                                waitForOpeningBracket = true;
                        }
                    }
                    else if (currentBlock.Type == BlockType.Interface || currentBlock.Type == BlockType.Internal)
                        currentBlock.AddChild(new Block(BlockType.Variable, null, currentBlock, line));
                    else if (currentBlock.Type == BlockType.Region)
                    {
                        switch (spaceSplit[0])
                        {
                            case "state":
                                newBlock = new Block(BlockType.State, spaceSplit[1], currentBlock);
                                break;
                            default:
                                throw new Exception("Invalid keyword in line " + lineNumber);
                        }

                        currentBlock.AddChild(newBlock);
                        currentBlock = newBlock;

                        if (newBlock.Name == "entry")
                            currentRegion.AddChild(new Block(BlockType.Entry, newBlock.UID, currentRegion));

                        if (!line.Contains("{"))
                            waitForOpeningBracket = true;
                    }
                    else if (currentBlock.Type == BlockType.State)
                    {
                        if (spaceSplit[0] == "every")
                        {
                            newBlock = new Block(BlockType.Repeat, null, currentBlock, line.Split('{')[0]);
                            currentBlock.AddChild(newBlock);
                            currentBlock = newBlock;

                            if (!line.Contains("{"))
                                waitForOpeningBracket = true;
                        }
                        else if (line.StartsWith("entry("))
                        {
                            newBlock = new Block(BlockType.StateEntry, null, currentBlock, line.Substring(5).Split('{')[0]);
                            currentBlock.AddChild(newBlock);
                            currentBlock = newBlock;

                            if (!line.Contains("{"))
                                waitForOpeningBracket = true;
                        }
                        else if (line.StartsWith("choice("))
                        {
                            newBlock = new Block(BlockType.Choice, null, currentBlock, line.Substring(5).Split('{')[0]);
                            currentBlock.AddChild(newBlock);
                            currentBlock = newBlock;

                            if (!line.Contains("{"))
                                waitForOpeningBracket = true;
                        }
                        else if (line.StartsWith("if("))
                        {
                            newBlock = new Block(BlockType.Condition, null, currentBlock, line.Substring(2).Split('{')[0]);
                            currentBlock.AddChild(newBlock);
                            currentBlock = newBlock;

                            if (!line.Contains("{"))
                                waitForOpeningBracket = true;
                        }
                        else
                            currentBlock.AddChild(new Block(BlockType.Operation, null, currentBlock, line));
                    }
                    else if (currentBlock.Type == BlockType.Choice)
                    {
                        if (spaceSplit[0] == "default")
                            currentBlock.AddChild(new Block(BlockType.DefaultChoice, null, currentBlock, spaceSplit[1]));
                        else if (line.StartsWith("choice("))
                        {
                            newBlock = new Block(BlockType.Choice, null, currentBlock, line.Substring(6).Split('{')[0]);
                            currentBlock.AddChild(newBlock);
                            currentBlock = newBlock;

                            if (!line.Contains("{"))
                                waitForOpeningBracket = true;
                        }
                        else if (line.StartsWith("if("))
                        {
                            newBlock = new Block(BlockType.Condition, null, currentBlock, line.Substring(2).Split('{')[0]);
                            currentBlock.AddChild(newBlock);
                            currentBlock = newBlock;

                            if (!line.Contains("{"))
                                waitForOpeningBracket = true;
                        }
                        else
                            currentBlock.AddChild(new Block(BlockType.Operation, null, currentBlock, line));
                    }
                    else if (currentBlock.Type == BlockType.Repeat || currentBlock.Type == BlockType.StateEntry)
                        currentBlock.AddChild(new Block(BlockType.Operation, null, currentBlock, line));
                    else if (currentBlock.Type == BlockType.Condition)
                    {
                        if (spaceSplit[0] == "jump")
                        {
                            if (currentBlock.SubBlocks.Any(x => x.Type == BlockType.Transition))
                                throw new Exception("A condition can only hold a single jump (line " + lineNumber + ")");

                            currentBlock.AddChild(new Block(BlockType.Transition, null, currentBlock, spaceSplit[1]));
                        }
                        else
                        {
                            currentBlock.AddChild(new Block(BlockType.Operation, null, currentBlock, line));
                        }
                    }

                    Console.WriteLine(line);
                }
            }

            return rootBlock;
        }

        private int FixConditionalTransitions()
        {
            Console.WriteLine("Applying constraint: \"DISALLOW_CONDITIONAL_TRANSITIONS\"...");
            int res = 0;

            List<String> events = new List<String>();
            foreach (Block blk in RootBlock.SubBlocks.First(x => x.Type == BlockType.Specification).SubBlocks)
            {
                String baseName = blk.Name;

                foreach (Block var in blk.SubBlocks.Where(x => x.Type == BlockType.Variable))
                    if (var.RealizeContents().StartsWith("event"))
                    {
                        String name = var.RealizeContents().Split(' ')[1].Split('=')[0];

                        if (baseName.Length == 0)
                            events.Add(name);
                        else
                            events.Add(baseName + "." + name);
                    }
            }

            foreach (Block region in Regions)
            {
                List<Transition> transitions = FindAllTransitions(region);
                foreach (Block state in region.SubBlocks.Where(x => x.Type == BlockType.State))
                {
                    List<Block> remove_from_state = new List<Block>();
                    List<Tuple<Block, Block>> add_block_to_block = new List<Tuple<Block, Block>>();
                    foreach (Block cond in state.SubBlocks.Where(x => x.Type == BlockType.Condition))
                    {
                        String cond_str = cond.RealizeContents();

                        if (events.Contains(cond_str))
                            continue;

                        Block DestinationBlock = state;
                        Block jump = cond.SubBlocks.FirstOrDefault(x => x.Type == BlockType.Transition);

                        if (jump != null)
                            DestinationBlock = region.SubBlocks.First(x => x.Type == BlockType.State && x.Name == jump.Text);


                        Console.WriteLine("{0} is against DISALLOW_CONDITIONAL_TRANSITIONS in state {1} (destination: {2})", cond_str, state.Name, DestinationBlock.Name);
                        res++;

                        if (cond_str.Contains("||") && !cond_str.Contains("&&"))
                        {
                            String[] splitted = cond_str.Split(new[] { "||" }, StringSplitOptions.None);

                            if (splitted.Count(x => events.Contains(x)) == splitted.Length)
                            {
                                Console.WriteLine("  -> Found fix (separating single transition to multiple ones)");

                                foreach (String s in splitted)
                                {
                                    Block new_condition = new Block(BlockType.Condition, null, state, s);
                                    new_condition.AddChild(new Block(BlockType.Transition, null, new_condition, DestinationBlock.Name));
                                    new_condition.Print(Console.Out);
                                    add_block_to_block.Add(new Tuple<Block, Block>(state, new_condition));
                                }

                                remove_from_state.Add(cond);
                                continue;
                            }
                        }

                        if (!events.Any(cond_str.Contains))
                        {
                            Console.WriteLine("  -> There are no trigger events involved in this transition, automatic fix is not possible");
                            continue;
                        }

                        IEnumerable<Transition> sameDestTrs = transitions.Where(x =>  x.SourceBlock.UID == state.UID);
                        if (sameDestTrs.Count() > 1)
                        {
                            Console.WriteLine("  -> There are multiple transitions targetting {0} (automatic fix may not be possible)", DestinationBlock.Name);

                            foreach (Transition t in sameDestTrs)
                                Console.WriteLine(":: {0} =({1})=> {2}", t.SourceBlock.Name, YakindufyTransition(t), t.DestinationBlock.Name);

                            if (sameDestTrs.Any(x => x.Condition != null && x.Condition.RealizeContents().Contains("||")))
                                Console.WriteLine("  -> Automatic fix is not possible, the condition has OR operator");
                            else
                            {
                                IEnumerable<IEnumerable<String>> _events = sameDestTrs.Where(x => x.Condition != null && !events.Contains(x.Condition.RealizeContents())).Select(x => x.Condition.RealizeContents().Split(new[] { "&&" }, StringSplitOptions.None).Where(y => events.Contains(y)));
                                List<String> _events_collected = new List<String>();
                                foreach (IEnumerable<String> k in _events)
                                    _events_collected.AddRange(k);

                                _events_collected = _events_collected.Distinct().ToList();

                                if (_events_collected.Count == 1 && !sameDestTrs.Where(x => x.Condition != null).Select(x => x.Condition.RealizeContents()).Where(x => events.Contains(x)).Contains(_events_collected[0]))
                                {
                                    Console.WriteLine("  -> There is only one trigger event involved [{0}]", _events_collected[0]);

                                    bool good = true;
                                    foreach (Transition tr1 in sameDestTrs)
                                        foreach (Transition tr2 in sameDestTrs)
                                            if (tr2.UID != tr1.UID && tr1.Condition != null && tr2.Condition != null)
                                                if (tr1.Condition.RealizeContents() == tr2.Condition.RealizeContents())
                                                {
                                                    if (tr1.WhatToDo.Count != tr2.WhatToDo.Count)
                                                    {
                                                        good = false;
                                                        Console.WriteLine("  -> Transitions with the same conditions does not have the same operations, automatic fix is not possible");
                                                        break;
                                                    }

                                                    Block[] tr1orderedops = tr1.WhatToDo.OrderBy(x => x.RealizeContents()).ToArray();
                                                    Block[] tr2orderedops = tr2.WhatToDo.OrderBy(x => x.RealizeContents()).ToArray();
                                                    for (int i = 0; i < tr1.WhatToDo.Count; i++)
                                                    {
                                                        if (tr1orderedops[i].RealizeContents() != tr2orderedops[i].RealizeContents())
                                                        {
                                                            good = false;
                                                            Console.WriteLine("  -> Transitions with the same conditions does not have the same operations, automatic fix is not possible");
                                                            break;
                                                        }
                                                    }
                                                }

                                    if (good)
                                    {
                                        Console.WriteLine("  -> Could not find conflicts, automatic fix is possible");
                                        Block new_condition = new Block(BlockType.Condition, null, state, _events_collected[0]);
                                        new_condition.AddChild(new Block(BlockType.Transition, null, new_condition, DestinationBlock.Name));
                                        new_condition.Print(Console.Out);
                                        add_block_to_block.Add(new Tuple<Block, Block>(state, new_condition));
                                        remove_from_state.Add(cond);

                                        Block new_entry = new Block(BlockType.StateEntry, null, state, cond_str.Replace("&&" + _events_collected[0], "").Replace(_events_collected[0] + "&&", ""));
                                        new_entry.SubBlocks.AddRange(cond.SubBlocks.Where(x => x.Type == BlockType.Operation));
                                        new_entry.Print(Console.Out);

                                        if (!DestinationBlock.SubBlocks.Any(x => x.Type == BlockType.StateEntry && x.RealizeContents() == new_entry.RealizeContents()))
                                            add_block_to_block.Add(new Tuple<Block, Block>(DestinationBlock, new_entry));
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("  -> There are multiple trigger events involved, moving transition condition");
                                    Console.WriteLine("     into state entry with entry condition. Please check the results manually");

                                    Block new_condition = new Block(BlockType.Condition, null, state, _events_collected[0]);
                                    new_condition.AddChild(new Block(BlockType.Transition, null, new_condition, DestinationBlock.Name));
                                    new_condition.Print(Console.Out);
                                    add_block_to_block.Add(new Tuple<Block, Block>(state, new_condition));
                                    remove_from_state.Add(cond);

                                    Block new_entry = new Block(BlockType.StateEntry, null, state, cond_str);
                                    new_entry.SubBlocks.AddRange(cond.SubBlocks.Where(x => x.Type == BlockType.Operation));
                                    new_entry.Print(Console.Out);

                                    if (!DestinationBlock.SubBlocks.Any(x => x.Type == BlockType.StateEntry && x.RealizeContents() == new_entry.RealizeContents()))
                                        add_block_to_block.Add(new Tuple<Block, Block>(DestinationBlock, new_entry));
                                }
                            }
                        }
                        else
                        {
                            Console.WriteLine("  -> There is only one transition to {0}, automatic fix is possible", DestinationBlock.Name);
                        }
                    }

                    foreach (Block blk in remove_from_state)
                        state.SubBlocks.Remove(blk);

                    foreach (Tuple<Block, Block> bblk in add_block_to_block)
                    {
                        if (bblk.Item2.Type == BlockType.Condition)
                            if (bblk.Item1.SubBlocks.Any(x => x.Type == BlockType.Condition && x.RealizeContents() == bblk.Item2.RealizeContents()))
                                continue;

                        bblk.Item1.AddChild(bblk.Item2);
                    }
                }
            }

            return res;
        }
    }
}
