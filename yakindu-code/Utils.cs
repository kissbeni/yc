﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace yakinducode
{
    public static class Utils
    {
        private static readonly Random rng = new Random();
        private static List<String> usedUIDs = new List<String>();

        public static String RandomUID()
        {
            const string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            String res;

            while (usedUIDs.Contains(res = new string(Enumerable.Repeat(chars, 22)
                .Select(s => s[rng.Next(s.Length)]).ToArray()))) ;

            usedUIDs.Add(res);
            return "_" + res;
        }

        public static Boolean CharDoesNotNeedSpace(Char ch)
        {
            return ch == '=' ||
                   ch == '-' ||
                   ch == '+' ||
                   ch == '(' ||
                   ch == ')' ||
                   ch == '<' ||
                   ch == '>' ||
                   ch == '&' ||
                   ch == '|' ||
                   ch == '^' ||
                   ch == '.' ||
                   ch == '{' ||
                   ch == '}';
        }

        public static String ExtractString(String line, Int32 n)
        {
            List<String> strs = new List<String>();
            String buf = "";

            int pos = 0;
            char stringChr = '\0';
            char prev = '\0';
            while (pos < line.Length)
            {
                char ch = line[pos];

                if (stringChr == ch && prev != '\\')
                {
                    stringChr = '\0';
                    strs.Add(buf);
                }
                else if ((ch == '\'' || ch == '"') && (stringChr == '\0'))
                    stringChr = ch;
                else if (stringChr != '\0')
                    buf += ch;

                prev = ch;
                pos++;
            }

            return strs[n];
        }

        public static String GenSpaces(int n)
        {
            return new string(Enumerable.Repeat(' ', n).ToArray());
        }

        // Extensions for XmlWriter
        public static void WriteXmiAttribute(this XmlWriter x, String name, String value)
        {
            x.WriteAttributeString("xmi", name, "http://www.omg.org/XMI", value);
        }

        public static void WriteXsiAttribute(this XmlWriter x, String name, String value)
        {
            x.WriteAttributeString("xsi", name, "http://www.w3.org/2001/XMLSchema-instance", value);
        }
    }
}
